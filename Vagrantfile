# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANT_NETWORK_IP = "192.168.33.200"
SERVER_NAME        = "devvm.local"

Vagrant.configure(2) do |config|
  config.vm.box = "chrislentz/trusty64-lamp"

  config.vm.network "forwarded_port", guest: 3306, host: 3306
  config.vm.network "private_network", ip: VAGRANT_NETWORK_IP

  config.vm.synced_folder ".", "/var/www/html", owner: "www-data", group: "www-data"
  config.vm.synced_folder ".", "/vagrant"

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "4096"
    vb.cpus = 4
    vb.name = "DevServer"
  end

  config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get -y install software-properties-common
    sudo apt-add-repository ppa:ansible/ansible
    sudo apt-get update
    sudo apt-get -y install ansible python-mysqldb php5-intl php5-xdebug mc adminer
  SHELL

  config.vm.provision "ansible_local" do |ansible|
    ansible.playbook = "/vagrant/builder.yml"
  end

  config.vm.provision "shell", run: "always", inline: <<-SHELL
    mkdir -p /vagrant/{backup,sites,logs}/
    databases=`mysql -u root -ppassword -e "SHOW DATABASES" | grep -v ^Database$ | grep -v ^.*_schema$ | grep -v ^mysql$`
    if [[ `echo -n $databases | wc -c` != 0 ]]; then
        mysqldump -u root -ppassword --databases --routines --triggers --single-transaction $databases | gzip > echo "/vagrant/backup/db-`date +%Y-%m-%d.%H:%M:%S`.gz"
        echo "Domain list:"
    fi
    sudo truncate --size 0 /etc/apache2/sites-available/develop.conf
    find /var/www/html/sites -mindepth 1 -maxdepth 1 -type d -not -name '.*' -print | while read directory; do
        domain=${directory#/var/www/html/sites/}
        echo "Use DevSite $domain" | sudo tee --append /etc/apache2/sites-available/develop.conf > /dev/null
        mysql -u root -ppassword -e "CREATE DATABASE IF NOT EXISTS $domain"
        echo http://$domain.devvm.local
    done
  SHELL
end
